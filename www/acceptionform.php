<?php
require './connect.php';

$method = $_GET['method'];
$role = $_GET['role'];
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
header("Content-type: application/json; charset=UTF-8");

// {
// 	"idcard": "1250100366411",
// 	"acceptionform" : "asds"
// }

if ($role === 'ผู้ป่วย' && $method === 'update_acceptionform') {
    $postdata = file_get_contents("php://input");
    $data = json_decode($postdata);
    $idCard = $data->idcard;

    $acceptionform = $data->acceptionform;

    $error = false;
    $sql = "UPDATE User_profile SET acceptionform=? WHERE person_id=?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('ss', $acceptionform, $idCard);
    $error = $stmt->execute();
	if ($error) {
        echo json_encode(array("result" => "Update acceptionform Success.."));
    } else {
        echo json_encode(array("result" => "Fail"));
    }
}

if (($role === 'ผู้ป่วย' || $role === 'หมอ') && $method === 'get_acceptionform') {
    $postdata = file_get_contents("php://input");
    $data = json_decode($postdata);
    $idCard = $data->idcard;
    $sql = "SELECT acceptionform FROM User_profile WHERE person_id = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('s', $idCard);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        echo json_encode($row);
    } else {
        echo json_encode(array("result" => "Fail"));
    }
}
